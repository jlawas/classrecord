/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classrecordapp;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author pn
 */
public class DatabaseConfiguration {
    private static final String uri = "jdbc:mysql://localhost:3306/classrecord";

    public DatabaseConfiguration() {
    }
    
    public static Connection getConnection() {
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(uri, "root", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }
}
