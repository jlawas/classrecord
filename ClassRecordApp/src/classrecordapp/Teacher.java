/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classrecordapp;

/**
 *
 * @author student
 */
public class Teacher extends Person {
   private int id;

    public Teacher() {
    }

    public Teacher(int id, String firstname, String lastname) {
        super(firstname, lastname);
        this.id = id;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   
   
}
