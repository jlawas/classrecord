/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classrecordapp;

/**
 *
 * @author student
 */
public class Student extends Person {
    private int id;
    private int yearLevel;

    public Student() {
    }

    public Student(int id, int yearLevel, String firstname, String lastname) {
        super(firstname, lastname);
        this.id = id;
        this.yearLevel = yearLevel;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYearLevel() {
        return yearLevel;
    }

    public void setYearLevel(int yearLevel) {
        this.yearLevel = yearLevel;
    }
    
    
    @Override 
    public String toString() {
    
    return String.format("%d %d",this.getId(),this.getYearLevel());
    }
}
